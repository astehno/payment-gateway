require! {
    \./db.ls : { transdb }
    \./config.json : { mode }
    \web3t : web3t-builder
    #\../web3t : web3t-builder
}

module.exports = {}

build-object = (mode, cb)->
    plugins = { eurs, usds }
    result = { mode, plugins }
    cb null, result
extend-object = (mode, cb)->
    mode.plugins = mode.plugins ? {}
    mode.plugins.eurs = eurs
    mode.plugins.usds = usds
    cb null, mode
extend-with-web3stable = (mode, cb)->
    return build-object mode, cb if typeof! mode is \String
    return extend-object mode, cb if typeof! mode is \Object
    cb "Not Supported Web3 Mode"


err, web3t <- web3t-builder mode
throw err if err?
module.exports <<<< web3t