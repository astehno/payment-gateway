require! {
    \http
    \https
    \cors
    \express
    \body-parser
    \express-validation : validate
    \express-rate-limit : limit
    \fs : { read-file-sync, read-file }
    \../validators/invoice-validator.ls
    \../config.json : { rate-limit, port, ip, portSSL, enable-static }
    \../invoice-request.ls
    \./rate-service.ls : { get-rates }
    \../log.ls : { trace }
}

app = express!

restify-callback = (res)-> (err, data)->
   trace \resp, err ? data
   return res.status 400 .send(err.message ? err) if err?
   res.status 200 .send data

app
  .use body-parser.json { limit: \50mb }
  .use limit rate-limit
  .use cors!
  .get \/rates, (req, res)->
      get-rates restify-callback(res)
  .post \/invoice/create, validate(invoice-validator), (req, res)->
      invoice-request req.body, restify-callback(res)
      
      


export start-http-service = (cb)->
    http.create-server(app).listen port, ip

export start-https-service = (cb)->
   key  = read-file-sync \./ssl/private.key
   cert = read-file-sync \./ssl/certificate.crt
   https.create-server({ key, cert }, app).listen portSSL, ip