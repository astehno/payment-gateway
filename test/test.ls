require! {
    \superagent : { get, post }
    \../config.json : { port }
}

url = "http://127.0.0.1:#{port}"

invoice = 
    amount            : \100
    amount-currency   : \USD
    callback-url      : \https://google.com.ua
    invoice-currency  : \BTC


err, data <- post "#{url}/invoice/create", invoice
console.log err, data?data
